const path = require('path');
const webpackBase = require('./webpack.base');

module.exports = {
    optimization: webpackBase.optimization,
    devtool: 'eval-source-map',
    mode: 'development',
    entry: {
        app: ['react-hot-loader/patch', path.resolve(__dirname, '../src/index.js')],
    },
    resolve: webpackBase.resolve,
    module: webpackBase.module,
    plugins: [
        webpackBase.plugins.htmlWebpack,
        webpackBase.plugins.nameModule,
        webpackBase.plugins.define,
        webpackBase.plugins.lodashModuleReplacement,
        webpackBase.plugins.antdDayjsWebpack,
        webpackBase.plugins.hardSourceWebpack,
        webpackBase.plugins.hotModuleReplacement,
        webpackBase.plugins.happyPackJsx,
        webpackBase.plugins.happyPackStyle,
    ],
    devServer: webpackBase.devServer,
    externals: webpackBase.externals,
};
