import React from 'react';
import { Iterable } from 'immutable';

const toJs = WrappedComponent => wrappedComponentProps => {
    const KEY = 0;
    const VALUE = 1;
    const propsJS = Object.entries(wrappedComponentProps).reduce((props, wrappedComponentProp) => {
        return {
            ...props,
            [wrappedComponentProp[KEY]]: Iterable.isIterable(wrappedComponentProp[VALUE])
                ? wrappedComponentProp[VALUE].toJS()
                : wrappedComponentProp[VALUE],
        };
    }, {});
    return <WrappedComponent {...propsJS} />;
};

export default toJs;
