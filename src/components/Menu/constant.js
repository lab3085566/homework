import React from 'react';
import { HomeOutlined} from '@ant-design/icons';
// menu
const memuData = [
    {
        key: 'list',
        link: '/list',
        name: 'Listing page',
        menuIcon: <HomeOutlined />,
    },
    {
        key: 'about',
        link: '/about',
        name: 'Watchlist page',
        menuIcon: <HomeOutlined />,
    },
];

export default memuData;
