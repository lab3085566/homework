import React from 'react';
import { shallow } from 'enzyme';
import { Summary } from '..';

describe(' Summary Component ', () => {
    it('should show correct text when value is not empty', () => {
        const wraper = shallow(<Summary value={2} />);
        expect(wraper.find('div').text()).toEqual('Total Count: 2');
    });

    it('should show empty text when value is empty', () => {
        const wraper = shallow(<Summary />);
        expect(wraper.text()).toEqual('');
    });
});
