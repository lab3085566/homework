import React from 'react';
import { shallow } from 'enzyme';
import About from '../index';

const findWrapper = (wrapper, tag) => {
    return wrapper.find(`[data-test="${tag}"]`);
};

describe('About', () => {
    it('should show despriction success', () => {
        const wraper = shallow(<About />);
        const ListElem = findWrapper(wraper, 'about');
        expect(ListElem.length).toBe(1);
        expect(wraper).toMatchSnapshot();
    });
});
