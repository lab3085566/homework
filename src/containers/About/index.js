import React, { PureComponent } from 'react';
import styles from './index.less';
import { Container, Row, Col, Jumbotron, Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import Product from '../Product';

export default class About extends PureComponent {
  constructor(props) {
    super(props);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      album: [],
    };
  }
  componentDidMount = () => {
    fetch('https://api.opensea.io/api/v1/assets?order_direction=desc&offset=0&limit=20').then(response => response.json()).then((data) => {
      console.log(data);
      console.log(data.assets);
      this.setState({
        album: data.assets,
      });

    });
  }

  render() {
    const { album, modal } = this.state;

    return (
      <div className="content">
        <Container>
         i am content
          <Row>
            {
              album.map(product => (
                <Col sm={6} md={4} className="mb-3">
                  <Product
                    product={product}
                  />
                </Col>
              ))
            }
          </Row>
        </Container>
      </div>
    );
  }
}