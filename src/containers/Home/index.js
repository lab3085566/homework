import React, { PureComponent, Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Layout } from 'antd';
import { createHashHistory } from 'history';
import { renderRoutes } from 'react-router-config';
import { hot } from 'react-hot-loader/root';
import Menu from '@components/Menu';
import routeConfig from '../../router/routeConfig';
import styles from './index.less';
import { message } from 'antd';

import {MetaMask} from '@contract/metamask'

const { Sider } = Layout;

const history = createHashHistory();

class Home extends PureComponent {
    componentDidMount() {
        // let self = this;
        // let metamask = window.metamask;
        // window.addEventListener('load', function() {
        //     let web3 = window.web3;
        //     if (typeof web3 !== 'undefined') {
        //         metamask.init();
        //         metamask.fetchAccounts();
        //         metamask.fetchNetwork();
        //         // self.Web3Interval = setInterval(() => metamask.fetchWeb3(), 1000);
        //         self.AccountInterval = setInterval(() => metamask.fetchAccounts(), 1000);
        //         self.NetworkInterval = setInterval(() => metamask.fetchNetwork(),  1000);
        //     } else {
        //         message.error("please install metamask!");
        //     }
        // })
    }
    render() {
        return (
            <BrowserRouter history={history}>
                <Layout className={styles.layoutSlder}>
                    <Sider theme="light">
                        <Menu />
                    </Sider>
                    <Suspense fallback={<div>Loading...</div>}>
                        {renderRoutes(routeConfig)}
                    </Suspense>
                </Layout>
            </BrowserRouter>
        );
    }
}
export default hot(Home);
