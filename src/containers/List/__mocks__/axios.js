/*
 * @Author: 
 * @Date: 2020-05-04 16:16:58
 * @LastEditors: 
 * @LastEditTime: 2020-05-08 23:21:56
 * @Description: file content
 */
const mockAxios = jest.genMockFromModule('axios');
mockAxios.create = jest.fn(() => mockAxios);

export default mockAxios;
