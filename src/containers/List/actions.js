/*
 * @Author: 
 * @Date: 2020-04-19 14:55:12
 * @LastEditors: 
 * @LastEditTime: 2020-09-13 16:53:54
 * @Description: file content
 */
import {
    FETCH_COUNTERLIST_LOADING,
    FETCH_COUNTERLIST_FAILURE,
    FETCH_COUNTERLIST_SUCCESS,
    INCREMENT,
    DECREMENT,
} from './actionTypes';
//import getInitList from '../../server';
import * as Web3 from 'web3'
import { OpenSeaPort, Network } from 'opensea-js'

export const increment = caption => ({
    type: INCREMENT,
    caption,
});

export const decrement = caption => ({
    type: DECREMENT,
    caption,
});

export const getCounterList = () => {
    return dispatch => {
        console.log("in connect wallet");
        const provider = new Web3.providers.HttpProvider('https://mainnet.infura.io');
        console.log(provider);

        const seaport = new OpenSeaPort(provider, {
          networkName: Network.Main
        })
    };
};



export function fetchNetwork() {
    const { web3 } = window;
    if (web3 !== null) {
      web3.version.getNetwork((err, netId) => {
        console.log('netId:',netId);
      });
    }
}
