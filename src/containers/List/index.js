import React, { PureComponent } from 'react';
import CounterPanel from '@components/CounterPanel';
import Summary from '@components/Summary';
import FetchError from '@components/FetchError';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import toJS from '@components/Hoc/toJSHOC';
import * as actions from './actions';
import styles from './index.less';

import {MetaMask} from '@contract/metamask'
//import '../../contract/metamask.js'

export class List extends PureComponent {
    componentDidMount() {
        // const { getCounterList } = this.props;
        // getCounterList();
        // window.metamask.init();
        fetch('https://api.opensea.io/api/v1/assets?order_direction=desc&offset=0&limit=20')
            .then(async response => {
                const data = await response.json();
                console.log(data);

                // check for error response
                if (!response.ok) {
                    // get error message from body or default to response statusText
                    const error = (data && data.message) || response.statusText;
                    return Promise.reject(error);
                }

                this.setState({ totalReactPackages: data.total })
            })
            .catch(error => {
                this.setState({ errorMessage: error.toString() });
                console.error('There was an error!', error);
            });

    }

    render() {
        const { isFetching, errorMessages, getCounterList } = this.props;
        if (isFetching) {
            return <p>Loading...</p>;
        }

        if (errorMessages) {
            return (
                <FetchError
                    message={errorMessages}
                    onRetry={() => {
                        //getCounterList();
                    }}
                />
            );
        }

        return (
            <div className={styles.counterListWrap}>
                <CounterPanel />
                <Summary />
            </div>
        );
    }
}

List.defaultProps = {
    isFetching: false,
    errorMessages: null,
    getCounterList: () => {},
};

List.propTypes = {
    isFetching: PropTypes.bool,
    errorMessages: PropTypes.string,
    getCounterList: PropTypes.func,
};

export default connect(
    state => {
        return {
            isFetching: state.getIn(['list', 'isFetching']),
            errorMessages: state.getIn(['list', 'errorMessages']),
        };
    },
    dispatch => ({ ...bindActionCreators(actions, dispatch) }),
)(toJS(List));
