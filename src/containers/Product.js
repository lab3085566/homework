import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Card, CardImg, CardBlock, CardTitle, CardSubtitle, CardText, Badge } from 'reactstrap';

export default class Product extends Component {
  static propTypes = {
    product: PropTypes.object,
  }

  render() {
    const { product} = this.props;

    return (
      <Card>
        <CardBlock>
          <CardTitle>{product.name}</CardTitle>
          <CardSubtitle>
            <h4>
              {
                <Badge color="success">ID：{product.id}</Badge>
              }
            </h4>
          </CardSubtitle>
        </CardBlock>
        <CardImg width="100%" src={product.image_url} alt="Card image cap" />
      </Card>
    );
  }
}
