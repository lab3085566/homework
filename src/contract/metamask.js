import React, { PureComponent } from 'react';
import * as Web3 from 'web3'
import { OpenSeaPort, Network } from 'opensea-js'
import { message } from 'antd';

class MetaMask  {
    state={
        web3: null,
        netId: 0
    }
    asset = {
        tokenAddress: "0x7dca125b1e805dc88814aed7ccc810f677d3e1db",
        tokenId: "1", // Token ID
    }

    constructor(){
    }

    init(){
        console.log("init");
        this.initWeb3();
    }
    initWeb3() {
        // This example provider won't let you make transactions, only read-only calls:
        window.web3 = new Web3(web3.currentProvider);
        this.state.web3 = window.web3;
        //const provider = new Web3.providers.HttpProvider('https://mainnet.infura.io')
        const provider = window.web3.currentProvider;
        console.log(provider);

        const seaport = new OpenSeaPort(provider, {
          networkName: Network.Main
        })
        console.log(seaport);
    }

    fetchAccounts() {
        if (this.state.web3 !== null) {
            //console.log("start fetchAccounts");
            this.state.web3.eth.getAccounts((err, accounts) => {
                if (err) {
                    message.error("Load metamask wallet error");
                } else {
                    if (accounts.length === 0) {
                        message.error("choose one MetaMask wallet by unlocking it");
                    }
                    //console.log(accounts);
                }
             });
        }
    }

    fetchNetwork() {
        if (this.state.web3 !== null) {
            this.state.web3.eth.net.getId((err, netId) => {
            //console.log('netId:',netId);
            if (err) {
                message.error("Network error, please check it");
            } else {
              // if network changed then change redux state
              if (netId !== this.state.netId) {
                  message.info("set netid");
                  this.state.netId = netId;
              }
            }
            });
        }
    }

};

window.metamask = new MetaMask()