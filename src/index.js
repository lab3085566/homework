import React from 'react';
import ReactDOM from 'react-dom';
import { ConfigProvider } from 'antd';
import { Provider } from 'react-redux';
import dayjs from 'dayjs';
import 'dayjs/locale/zh-cn';
import locale from 'antd/es/locale/zh_CN';
import Home from './containers/Home';
import store from './store';
import './index.less';



dayjs.locale('zh-cn');

const App = () => (
    <ConfigProvider locale={locale}>
        <Provider store={store}>
            <Home />
        </Provider>
    </ConfigProvider>
);

const render = () => {
    const root = document.getElementById('root');
    ReactDOM.render(<App />, root);
};

render();

export default App;
