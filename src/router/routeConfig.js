import { lazy } from 'react';
import { Redirect } from 'react-router-dom';

const About = lazy(() => import(/* webpackChunkName: "work" */ '../containers/About'));
const List = lazy(() => import(/* webpackChunkName: "list" */ '../containers/List'));

export default [
    {
        path: '/about',
        component: About,
        exact: true,
    },
    {
        path: '/list',
        component: List,
        exact: true,
    },
    {
        path: '/',
        component: List,
        // eslint-disable-next-line react/display-name
        render: () => <Redirect to="/list" />,
    },
];
